import App from './App'
import uView from '@/uni_modules/uview-ui'
import store from '@/store/store.js'

// 按需导入 $http 对象
import {
  $http
} from '@escook/request-miniprogram'
// 在 uni-app 项目中，可以把 $http 挂载到 uni 顶级对象之上，方便全局调用
uni.$http = $http
// 请求的根路径
$http.baseUrl = 'https://zy.nmgdcjg.com'
// $http.baseUrl = 'http://wans.ink:9090'  
Vue.use(uView)

const sm4 = require("miniprogram-sm-crypto").sm4;
const crypt_enabled = 0; //1:加密  0:不加密
const key = 'YHP90noOeIft2vu4'

function StringToHex(str) {
  if (str == '')
    return '';
  let hex = [];
  for (var i = 0; i < str.length; i++) {
    hex.push((str.charCodeAt(i)).toString(16));
  }
  return hex.join('');
}

function base64_encode(str) { // 编码，配合encodeURIComponent使用
  var c1, c2, c3;
  var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
  var i = 0,
    len = str.length,
    strin = '';
  while (i < len) {
    c1 = str.charCodeAt(i++) & 0xff;
    if (i == len) {
      strin += base64EncodeChars.charAt(c1 >> 2);
      strin += base64EncodeChars.charAt((c1 & 0x3) << 4);
      strin += "==";
      break;
    }
    c2 = str.charCodeAt(i++);
    if (i == len) {
      strin += base64EncodeChars.charAt(c1 >> 2);
      strin += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
      strin += base64EncodeChars.charAt((c2 & 0xF) << 2);
      strin += "=";
      break;
    }
    c3 = str.charCodeAt(i++);
    strin += base64EncodeChars.charAt(c1 >> 2);
    strin += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
    strin += base64EncodeChars.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
    strin += base64EncodeChars.charAt(c3 & 0x3F)
  }
  return strin
}

function base64_decode(input) { // 解码，配合decodeURIComponent使用
  var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
  var output = "";
  var chr1, chr2, chr3;
  var enc1, enc2, enc3, enc4;
  var i = 0;
  input = input.replace(/[^A-Za-z0-9+/=]/g, "");
  while (i < input.length) {
    enc1 = base64EncodeChars.indexOf(input.charAt(i++));
    enc2 = base64EncodeChars.indexOf(input.charAt(i++));
    enc3 = base64EncodeChars.indexOf(input.charAt(i++));
    enc4 = base64EncodeChars.indexOf(input.charAt(i++));
    chr1 = (enc1 << 2) | (enc2 >> 4);
    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
    chr3 = ((enc3 & 3) << 6) | enc4;
    output = output + String.fromCharCode(chr1);
    if (enc3 != 64) {
      output = output + String.fromCharCode(chr2);
    }
    if (enc4 != 64) {
      output = output + String.fromCharCode(chr3);
    }
  }
  return utf8_decode(output);
}


function utf8_decode(utftext) { // utf-8解码
  var string = '';
  let i = 0;
  let c = 0;
  let c1 = 0;
  let c2 = 0;
  while (i < utftext.length) {
    c = utftext.charCodeAt(i);
    if (c < 128) {
      string += String.fromCharCode(c);
      i++;
    } else if ((c > 191) && (c < 224)) {
      c1 = utftext.charCodeAt(i + 1);
      string += String.fromCharCode(((c & 31) << 6) | (c1 & 63));
      i += 2;
    } else {
      c1 = utftext.charCodeAt(i + 1);
      c2 = utftext.charCodeAt(i + 2);
      string += String.fromCharCode(((c & 15) << 12) | ((c1 & 63) << 6) | (c2 & 63));
      i += 3;
    }
  }
  return string;
}


// 请求开始之前做一些事情
$http.beforeRequest = function(options) {
  // debugger
  // console.log("Requestoptions", options)
  // console.log("key", key)
  if (crypt_enabled == 1) {
    uni.showLoading({
      title: '数据加载中...',
    })
    let encryptData = sm4.encrypt(JSON.stringify(options.data), StringToHex(key), {
      mode: 'cbc',
      padding: 'pkcs#5',
      // key倒序
      iv: StringToHex(key.split('').reverse().join(''))
    })
    // Base64编码
    // console.log("encryptData", encryptData.toString())
    let encryptStr = base64_encode(encryptData.toString())
    options.data = encryptStr;

  } else {
    	options.header = {
    			// 字段的值可以直接从 vuex 中进行获取
    			Authorization: 'Bearer ' + store.state.m_user.token,
          comId:store.state.m_user.comId,
    		}
        // if(options.url != (options.baseUrl + "/learn/wx/public/addLearnRecord")){
        //   uni.showLoading({
        //     title: '数据加载中...',
        //   })
        // }
  }
  // console.log("加密后密文：\n" + encryptStr);
  // console.log("encryptData", encryptData);
}

// 响应拦截器
$http.afterRequest = function(options) {
	// token失效返回登录页面
  if ([401, 403].indexOf(options.data.code) !== -1 ) {
	  uni.navigateTo({
	  	url: '/pages/login/login'
	  })
  }
  if (crypt_enabled == 1) {
    uni.hideLoading()
    if (options.data.data == null) {
      uni.hideLoading()
    } else {
      // Base64解码
      let encrypt = base64_decode(options.data.data);
      // console.log("解码密文：\n" + encrypt);
      let decryptData = sm4.decrypt(encrypt, StringToHex(key), {
        mode: 'cbc',
        padding: 'pkcs#7',
        // key倒序
        iv: StringToHex(key.split('').reverse().join(''))
      });
      options.data.data = JSON.parse(decryptData)
      uni.hideLoading()
    }
  } else {
    uni.hideLoading()
  }
  // console.log("解密后明文：\n" + decryptData);
}


// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
  ...App,
  store,
})
app.$mount()
// #endif

// #ifdef VUE3
import {
  createSSRApp
} from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif
