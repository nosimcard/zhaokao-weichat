// 导出一个 mixin 对象
export default {
  data() {
    return {
      statusWh: 0, //显示屏幕的高度
      statusBarHeight: 0, //状态栏高度
      navBarHeight: 45, // 导航栏高度 
      screenWidth: 0, //屏幕的宽度
      windowWidth: 0, //顶部去掉胶囊后剩的位置
    };
  },
  onShow() {
    // 在页面刚展示的时候，设置数字徽标
    this.setBadge()
  },
  methods: {
    setBadge() {
      const info = uni.getSystemInfoSync()
      this.statusWh = info.windowHeight; //显示屏幕的高度
      // 设置状态栏高度（H5顶部无状态栏小程序有状态栏需要撑起高度）
      this.statusBarHeight = info.statusBarHeight;
      this.screenWidth = info.screenWidth;
      // 获取胶囊的位置信息
      const menuButtonInfo = uni.getMenuButtonBoundingClientRect();
      // (胶囊底部高度 - 状态栏的高度) + (胶囊顶部高度 - 状态栏内的高度) = 导航栏的高度
      this.navBarHeight = (menuButtonInfo.bottom - info.statusBarHeight) + (menuButtonInfo.top - info.statusBarHeight)
      // 顶部去掉胶囊后剩的位置
      this.windowWidth = menuButtonInfo.left
    },
  },
}