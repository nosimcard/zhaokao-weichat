export default {
  // 为当前模块开启命名空间
  namespaced: true,

  // 模块的 state 数据
  state: () => ({
    // 购物车的数组，用来存储购物车中每个商品的信息对象
    // 每个商品的信息对象，都包含如下 6 个属性：
    // { chapterName, questionDbId, questionType, questionLevel, questionNum, questionScore }
    zhangJie: [],
    jianCeTest: {}
  }),

  // 模块的 mutations 方法
  mutations: {
    addTozhangJie(state, goods) {
      // 根据提交的商品的Id，查询购物车中是否存在这件商品
      // 如果不存在，则 findResult 为 undefined；否则，为查找到的商品信息对象
      const findResult = state.zhangJie.find((x) => x.id === goods.id)

      if (!findResult) {
        // 如果购物车中没有这件商品，则直接 push
        state.zhangJie.push(goods)
      } else {
        // 如果购物车中有这件商品，则只更新数量即可
        // findResult.goods_count++
        findResult.chapterName = goods.chapterName
        findResult.questionDbId = goods.questionDbId
        findResult.questionType = goods.questionType
        findResult.questionLevel = goods.questionLevel
        findResult.questionNum = goods.questionNum
        findResult.questionScore = goods.questionScore
      }
    },

    addToJianCe(state, data) {
      state.jianCeTest = data;
    },

    // 根据 chapterName 从章节列表中删除对应的章节
    removeZhangJieByName(state, id) {
      // 调用数组的 filter 方法进行过滤,filter()方法会创建一个新数组
      state.zhangJie = state.zhangJie.filter(x => x.id !== id)
      // // 持久化存储到本地
      // this.commit('m_cart/saveToStorage')
    },

    clearStateZhangJie(state) {
      state.zhangJie = [];
    },



  },

  // 模块的 getters 属性
  getters: {},
}
