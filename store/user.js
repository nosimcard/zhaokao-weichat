export default {
  // 为当前模块开启命名空间
  namespaced: true,

  // 模块的 state 数据
  state: () => ({
    token: uni.getStorageSync('token') || '',
    comId: uni.getStorageSync('comId') || '',
    code: "", //wx.login获取
    openId: uni.getStorageSync('openId') || '',
    userId: uni.getStorageSync('userId') || '',
    branchId: uni.getStorageSync('branchId') || '',
  }),

  // 模块的 mutations 方法
  mutations: {
    // wx.login获取的code
    getWxCodeStore(state, data) {
      console.log("data", data)
      state.code = data;
    },

    // 微信用户登录获取openId
    getTokenStore(state, data) {
      console.log("data",data)
      state.token = data;
      // 通过 commit 方法，调用 m_cart 命名空间下的 saveToStorage 方法
      this.commit('m_user/saveToTokenStorage')
    },
    
    // 微信用户登录获取comId
    getComIdStore(state, data) {
      console.log("data",data)
      state.comId = data;
      // 通过 commit 方法，调用 m_cart 命名空间下的 saveToStorage 方法
      this.commit('m_user/saveToComIdStorage')
    },
    
    // 将userID数据持久化存储到本地
    saveToComIdStorage(state) {
      uni.setStorageSync('comId', state.comId)
    },

    // 微信用户登录获取branchId
    getWxBranchIdStore(state, data) {
      state.branchId = data.branchId;
      state.userId = data.userId;
      // 通过 commit 方法，调用 m_cart 命名空间下的 saveToStorage 方法
      this.commit('m_user/saveBranchIdToStorage')
    },

    // 将userID数据持久化存储到本地
    saveToTokenStorage(state) {
      uni.setStorageSync('token', state.token)
    },

    // 将userID数据持久化存储到本地
    saveBranchIdToStorage(state) {
      uni.setStorageSync('branchId', state.branchId)
      uni.setStorageSync('userId', state.userId)
    }

    // // 微信用户登录获取openId
    // getWxOpenIdStore(state, data) {
    //   state.openId = data.openId;
    // },

  },

  // 模块的 getters 属性
  getters: {},
}
