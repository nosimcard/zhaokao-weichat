const TokenKey = 'App-Admin-Token'

const PasswordKey = 'Login-Password'

const UserNameKey = 'Login-username'

const NickNameKey = 'Nick-Name'

const PhoneNumKey = 'Phone-Num'

export function getToken() {
	return uni.getStorageSync(TokenKey)
}

export function setToken(token) {
	return uni.setStorageSync(TokenKey, token)
}

export function removeToken() {
	return uni.removeStorageSync(TokenKey)
}


export function getPassword() {
	return uni.getStorageSync(PasswordKey)
}

export function setPassword(password) {
	return uni.setStorageSync(PasswordKey, password)
}

export function removePassword() {
	return uni.removeStorageSync(PasswordKey)
}

export function getLoginUserName() {
	return uni.getStorageSync(UserNameKey)
}

export function setLoginUserName(loginName) {
	return uni.setStorageSync(UserNameKey, loginName)
}

export function removeLoginUserName() {
	return uni.removeStorageSync(UserNameKey)
}

export function getNickName() {
	return uni.getStorageSync(NickNameKey)
}

export function setNickName(nickName) {
	return uni.setStorageSync(NickNameKey, nickName)
}

export function removeNickName() {
	return uni.removeStorageSync(NickNameKey)
}

export function getPhoneNum() {
	return uni.getStorageSync(PhoneNumKey)
}

export function setPhoneNum(num) {
	return uni.setStorageSync(PhoneNumKey, num)
}

export function removePhoneNum() {
	return uni.removeStorageSync(PhoneNumKey)
}